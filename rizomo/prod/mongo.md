# Install Percona Mongodb Operator

(These instructions are for reference, and not meant for automation, refer to original documentation for up to date commands)

https://docs.percona.com/percona-operator-for-mongodb/kubernetes.html


```
git clone -b v1.13.0 https://github.com/percona/percona-server-mongodb-operator
cd percona-server-mongodb-operator/
kubectl apply -f deploy/crd.yaml
kubectl -n rizomo-prod apply -f deploy/rbac.yaml
kubectl -n rizomo-prod apply -f deploy/operator.yaml
```
