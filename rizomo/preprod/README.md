# Install

## Create AK/SK for oos

OOS: Outscale Object Store
AK: Access Key
SK: Secret Key

First, you need to create credentials to interact with OOS.

Send a mail to outscale support:
```
Can you create a user with this email:
And attach it to this account:
SNC cloudgouv with User id:
```

Then, log in the UI and generate a pair of AK/SK.

Finally configure your tools with this credentials. Example with mc cli:

```
{
	"version": "10",
	"aliases": {
		"oos": {
			"url": "https://oos.cloudgouv-eu-west-1.outscale.com",
			"accessKey": "AK",
			"secretKey": "SK",
			"api": "s3v4",
			"path": "auto"
		}
	}
}
```

## Create buckets

With an oos accound and aws cli configured:

```
aws s3api create-bucket \
    --profile oos \
    --bucket rizomo-preprod \
    --acl public-read \
    --endpoint https://oos.cloudgouv-eu-west-1.outscale.com

aws s3api create-bucket \
    --profile oos \
    --bucket rizomo-preprod-backups \
    --endpoint https://oos.cloudgouv-eu-west-1.outscale.com
```

## Postgres

Then, we install a HA postgres instance:

```
kubectl apply -f ./pg.yml
```

## Mongo

Install [Percona Mongo Operator](mongo.md).

Then we can deploy our HA mongodb instance:
```
export MONGO_PASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 18 | head -n 1)
kubectl -n rizomo-preprod create secret generic rizomo-app --from-literal=MONGO_PASSWORD=$MONGO_PASSWORD
kubectl -n rizomo-preprod apply -f ./mongo.yaml
```

## Keycloak

Use helm to deploy Keycloak:

```
helm upgrade --install -n rizomo-preprod keycloak codecentric/keycloak -f kc.yaml -f /path/to/rizomo/secrets/preprod/kc.yaml
```

## Rizomo

To deploy Rizomo, you can use helm:
```
kubectl -n rizomo-preprod apply -f ../rizomo-cm.yaml
helm upgrade --install laboite eole/laboite -n rizomo-preprod -f rizomo.yaml -f /path/to/rizomo/secrets/preprod/rizomo.yaml
```

## Questionnaire
```
helm upgrade --install questionnaire eole/questionnaire -n rizomo-preprod -f ./questionnaire.yaml
```

And then, in keycloak, in the sso client, add questionaire url to the list of valid redirect URIs.
