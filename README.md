# Rizomo prod

## Installation

### Cluster

[Install Instructions](cluster/README.md)

### Rizomo Prod

[Install Instructions](prod/README.md)

### Rizomo PreProd

[Install Instructions](preprod/README.md)

## ssh to kubernetes nodes

ssh to bastion host, and then:
```
k get no -o wide # find the ip of the node you want
ssh outscale@10.0.0.x
```

## IaaS

https://cockpit-cloudgouv-eu-west-1.outscale.com/