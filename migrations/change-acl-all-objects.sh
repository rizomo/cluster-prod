#!/bin/bash

acl=public-read
bucket=rizomo-prod
aws_command="aws s3api --endpoint https://oos.cloudgouv-eu-west-1.outscale.com --profile oos "

$aws_command list-objects --bucket $bucket | jq -r ".Contents[].Key" > /tmp/list

cat /tmp/list | while read object
do
  echo -e "set acl of \033[31m $object \033[0m as $acl"
  $aws_command put-object-acl --bucket $bucket --key "$object" --acl $acl
done
