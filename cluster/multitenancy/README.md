# Cluster access and multitenancy

Multiple teams can use the cluster and create their namespaces. Each team should only have access to their namespaces and each namespaces should be isolated from each others..

Each of those teams have ssh access to the jump host in their own user space and have their own kubeconfig. 

## How to kubectl from your local machine

The cluster API is not open to the public and is only available through the jump host.
You can make an ssh tunnel to access the cluster API from your local machine.

Follow those steps to create a [systemd service](lsh-proxy@.service) and set up a tunnel for your clusters.

```
cp lsh-proxy@.service ~/.config/systemd/user/lsh-proxy@.service
systemctl --user enable lsh-proxy@margaret-hamilton
systemctl --user start lsh-proxy@margaret-hamilton
systemctl --user status lsh-proxy@margaret-hamilton
```

You can now use kubectl from your machine. 

## RBAC

All teams have a set of permissions that are defined in the following [kubernetes roles](rbac.yaml).
When a user creates a namespace the role is automatically assigned to the user group by a [Kyverno ClusterPolicy](clusterpolicy.yaml).

**limitations**: a user can only have one group/team for this automation to work.

If more roles are needed for your team, you can create an issue/MR here or ask in the dedicated room in Tchap. 

You can see all available resources with: `kubectl api-resources`

## Process for user creation:

Actual process is based on [kubernetes certificate authentification](https://kubernetes.io/docs/reference/access-authn-authz/certificate-signing-requests/#normal-user)

### Create certificate
```
export USER=
export GROUP=
openssl genrsa -out $USER.key  4096

cat <<EOF > $USER.csr.cnf
[ req ]
default_bits = 2048
prompt = no
default_md = sha256
distinguished_name = dn
[ dn ]
CN = $USER              
O = $GROUP
[ v3_ext ]                                
authorityKeyIdentifier=keyid,issuer:always
basicConstraints=CA:FALSE                
keyUsage=keyEncipherment,dataEncipherment
extendedKeyUsage=clientAuth
EOF

openssl req -config $USER.csr.cnf -new -key $USER.key -nodes -out $USER.csr
```

### Create a CertificateSigningRequest

```
export BASE64_CSR=$(cat $USER.csr | base64 | tr -d '\n')
cat <<EOF > user-request-$USER.yml
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: user-request-$USER
spec:
  request: $BASE64_CSR
  signerName: kubernetes.io/kube-apiserver-client
  expirationSeconds: 31556952   # one year
  usages:
  - client auth
EOF

k apply -f ./user-request-$USER.yml
```

`CertificateSigningRequest` needs to be approved
```
kubectl certificate approve user-request-$USER
```

Certificate should now be signed. Check `CertificateSigningRequest` is Approved and Issued. 

You can download the new signed public key from the csr resource:
```
kubectl get csr user-request-$USER -o jsonpath='{.status.certificate}' | base64 --decode > $USER.crt
```

### Create user kubeconfig

Get certificate-authority in a file `./kubernetes.ca.crt` (the field `certificate-authority-data` in your current kube config).

```
export CLUSTER_IP=
export CLUSTER_PORT=
export CLUSTER_NAME
kubectl --kubeconfig ~/.kube/config-$USER config set-cluster $CLUSTER_NAME --certificate-authority=./kubernetes.ca.crt
 --server=https://$CLUSTER_IP:$CLUSTER_PORT
kubectl --kubeconfig ~/.kube/config-$USER config set-credentials $USER --client-certificate=$USER.crt --client-key=$USER.key --embed-certs=true
kubectl --kubeconfig ~/.kube/config-$USER config set-context default --cluster=$CLUSTER_NAME --user=$USER
kubectl --kubeconfig ~/.kube/config-$USER config use-context default
```

### Add user to ClusterRoleBinding

To enable user to create namespaces, add it to [ClusterRoleBinding](users/rbac.yaml)
