# Instructions to create production cluster for Rizomo

## Introduction

### Requirements

We assume that if you read this documentation, you have a basic understanding of the following concepts:

- linux
- linux containers (docker/runc/containerd/cri-o)
- APIs and declarative APIs
- kubernetes (k8s)
  - API
  - Controllers
  - Cluster API
- IaaS and cloud
  - outscale/aws
  - VM images
  - VPC/security-groups
  - load balancers
  - volumes
- CSI (Container Storage Interface)
- CNI (Container Network Interface)

### Goal

If you follow carefully these instructions, at the end, you will have a running instance of Rizomo on a kubernetes cluster running on outscale.

### Components

- To provision the base infrastructure we use [Outscale Cluster API provider](https://github.com/outscale-dev/cluster-api-provider-outscale).
- For the state, we rely on:
  - Outscale Object Store
  - [Zalando postgres operator](https://github.com/zalando/postgres-operator) to deploy HA postgres databases with backups
  - [Percona MongoDB operator](https://docs.percona.com/percona-operator-for-mongodb/index.html) to provide HA mongo databases with backups
- For the rest, we use [libre.sh](https://gitlab.mim-libre.fr/EOLE/eole-3/tools) to deploy and manage:
  - nginx-ingress
  - cert-manager
  - Object Store
    - minio operator
    - minio tenant
  - libre.sh operator
    - bucket operator
    - Databases
      - Redis Cluster
      - Postgres Cluster (With Object Store Backups, PITR)
  - observability
    - grafana
    - prometheus (thanos)
    - loki (promtail)
  - velero for backups

## Clusters Topology

We currently have 3 clusters deployed.
For each cluster, there is a folder with its name, and resources that are/were used to deploy this cluster.
(most yaml are the for reference).

### Preprod - `margaret-hamilton`

Our preprod cluster is historically called `rizomo-prod` and renamed into `margaret-hamilton`.
This cluster is self managed. It means that cluster-api objects are deployed on the cluster, and cluster-api controller are running on the same cluster.

It auto manages it self.

### Prod

For production we have 2 clusters:

#### Management - `simone-veil`

This cluster only has 3 small control-plane nodes.

#### Workload - `hedy-lamarr`

This is the current production cluster.

## Getting started

### Bootstrap cluster

To get started, we need a k8s API, to be able to leverage Cluster API.
We’ll do that on [kind](kind.md) on the bastion host.

Then we can start our bootstrap cluster:

```
kind create cluster --image=kindest/node:v1.29.12
kubectl cluster-info
```

### Install clusterctl

Once we have a running cluster, we need to prepare it to be able to create our workload cluster.
We need first to install [clusterctl](clusterctl.md).

Then we can make our bootstrap cluster ready to create workload clusters:
```
clusterctl init --infrastructure outscale
```

### Create workload cluster

We can now create a workload cluster, in our case Rizomo prod cluster.
(Create a ssh key beforehand in outscale, here it is with the name pierreozoux).

```
export OSC_REGION=cloudgouv-eu-west-1
export OSC_IOPS=1500
export OSC_VOLUME_SIZE=50
export OSC_KEYPAIR_NAME=pierreozoux
export OSC_SUBREGION_NAME=cloudgouv-eu-west-1a
export OSC_VM_TYPE=tinav6.c2r4p2
export OSC_IMAGE_NAME=ubuntu-2204-2204-kubernetes-v1.28.5-2024-01-17
export OSC_VOLUME_TYPE=io1

clusterctl generate cluster \
    rizomo-prod \
    --kubernetes-version 1.27.9 \
    --control-plane-machine-count=3 \
    --target-namespace=rizomo-prod \
    --worker-machine-count=3 > ./cluster-rizomo-prod.yaml

kubectl create ns rizomo-prod
kubectl -n rizomo-prod apply -f cluster-rizomo-prod.yaml
```

```
export OSC_REGION=eu-west-2
export OSC_IOPS=1500
export OSC_VOLUME_SIZE=50
export OSC_KEYPAIR_NAME=pierreozoux
export OSC_SUBREGION_NAME=eu-west-2a
export OSC_VM_TYPE=tinav6.c2r4p2
export OSC_IMAGE_NAME=ubuntu-2204-2204-kubernetes-v1.28.5-2024-01-10
export OSC_VOLUME_TYPE=io1

clusterctl generate cluster \
    suite-staging \
    --kubernetes-version 1.28.5 \
    --control-plane-machine-count=3 \
    --target-namespace=suite-staging \
    --worker-machine-count=3 > ./cluster-suite-staging.yaml

kubectl create ns rizomo-prod
kubectl -n rizomo-prod apply -f cluster-rizomo-prod.yaml
```


This command is just a reference to explain how this file was created, but [cluster-rizomo-prod.yaml](cluster-rizomo-prod.yaml) is in this git repo, and is now disconnected from the way it was created.

Examples to get inspiration:
https://github.com/outscale/cluster-api-provider-outscale/tree/main/examples

### Get kubeconfig

```
export CLUSTER_NAME=suite-horsprod
export WORKLOAD_CLUSTER_KUBECONFIG=/tmp/workload-kubeconfig
clusterctl -n $CLUSTER_NAME get kubeconfig $CLUSTER_NAME > $WORKLOAD_CLUSTER_KUBECONFIG
export KUBECONFIG=$WORKLOAD_CLUSTER_KUBECONFIG
```

### Install CNI in workload cluster

At this point, node will not be Ready, you need to install a CNI for that to change.

[Cilium](https://docs.cilium.io) is now the defacto CNI for kubernetes, relying on eBPF for networking and monitoring of networks.

First, we’ll need to install [cilium](cilium-cli.md) cli.

Then, we can install cilium:
```
helm repo add cilium https://helm.cilium.io/
helm upgrade --install cilium cilium/cilium --version 1.16.3 --namespace kube-system -f cilium.yaml 
cilium status --wait
cilium connectivity test
```

This command is just a reference to explain how cilium.yaml was created, but [cilium.yaml](cilium.yaml) is in this git repo, and is now disconnected from the way it was created.

After installing the CNI, it is [recommended to restart all pods that were running previously](), like `kube-apiserver` or the others:
```
kubectl get pods --all-namespaces -o custom-columns=NAMESPACE:.metadata.namespace,NAME:.metadata.name --no-headers=true | awk '{print "-n "$1" "$2}' | grep kube-system | grep -v cilium | xargs -L 1 -r kubectl delete pod
```
(If not you might have errors deploy ingress for instance `failed calling webhook "validate.nginx.ingress.kubernetes.io"`.)

#### Security group issue

There is currently a [bug in outscale-cluster-api](https://github.com/outscale/cluster-api-provider-outscale/issues/322).
In the mean time, you need to add this rule to the `name-securitygroup-kw-xxx` sg:
```
      {
        "flow": "Inbound",
        "fromPortRange": 10250,
        "ipProtocol": "tcp",
        "ipRange": "10.0.4.0/24",
        "name": "x-securitygrouprule-api-kubelet-kcp",
        "toPortRange": 10250
      },
```

### Install CCM in workload cluster

Create EIM user and policy and link it.

CCM is the Cloud Controller Manager, it allows to keep in sync VMs tags, and create cloud load balancer when you create k8s load balancer services.

```
git clone https://github.com/outscale-dev/cloud-provider-osc
cd cloud-provider-osc/
cp deploy/secrets.example.yml secrets.yml
vi secrets.yml # and edit accordingly
k apply -f ./secrets.yml
helm upgrade --install -n kube-system --wait --wait-for-jobs k8s-osc-ccm deploy/k8s-osc-ccm --set oscSecretName=osc-secret-ccm
osc-cli api CreateApiAccessRule --IpRanges '["142.44.48.168/32"]' # get the IP of the Internet Gateway of the VPC of the cluster
```

### Install CSI in workload cluster

CSI is the container storage interface, and it allows to create and map volumes to the right VM in your cloud provider when you create k8s PVCs, and bind them to k8s nodes.

```
git clone https://github.com/outscale-dev/osc-bsu-csi-driver.git
cd osc-bsu-csi-driver/
git checkout v1.4.1
vi deploy/kubernetes/secret.yaml # and edit accordingly
k apply -f deploy/kubernetes/secret.yaml
helm upgrade --install osc-bsu-csi-driver ./osc-bsu-csi-driver     --namespace kube-system     --set enableVolumeScheduling=true     --set enableVolumeResizing=true     --set enableVolumeSnapshot=true     --set region=cloudgouv-eu-west-1|eu-west-2
cp ./examples/kubernetes/storageclass/specs/example.yaml ./storage-class.yaml
kubectl apply -f ./storage-class.yaml
```

This command is just a reference to explain how storage-class.yaml was created, but [storage-class.yaml](storage-class.yaml) is in this git repo, and is now disconnected from the way it was created.

### MGMT cluster

If you need to run some load on mgmt cluster, untaint the control-plane:
```
kubectl taint node ip-10-0-0-27.eu-west-2.compute.internal node-role.kubernetes.io/control-plane:NoSchedule-
```

### libre.sh

For these componenents:
  - nginx-ingress
  - cert-manager
  - Object Store
    - minio operator
    - minio tenant
  - libre.sh operator:
    - bucket operator
    - Databases
      - Redis Cluster
      - Postgres Cluster (With Object Store Backups, PITR)
  - observability
    - grafana
    - prometheus (thanos)
    - loki (promtail)
  - velero for backups

Head over the upstream repo to see how it installed.

https://forge.liiib.re/indiehost/libre.sh/libre.sh/-/tree/develop?ref_type=heads#minimal-install

### Move bootstrap/mgmt cluster into workload cluster

The procedure is documented [upstream here](https://cluster-api-cloudstack.sigs.k8s.io/topics/mover.html).

First, you need to initialize the workload cluster to be able to host:

```
export WORKLOAD_CLUSTER_KUBECONFIG=/tmp/workload-kubeconfig
clusterctl -n rizomo-prod get kubeconfig rizomo-prod > $WORKLOAD_CLUSTER_KUBECONFIG
export KUBECONFIG=$WORKLOAD_CLUSTER_KUBECONFIG
cd cluster-api-provider-outscale
export OSC_ACCESS_KEY=<your-access-key>
export OSC_SECRET_KEY=<your-secret-access-key>
export OSC_REGION=cloudgouv-eu-west-1
make credential
clusterctl init --infrastructure outscale
```

And then, you just can move:
```
unset KUBECONFIG
clusterctl -n rizomo-prod describe cluster rizomo-prod
clusterctl -n rizomo-prod move --to-kubeconfig /tmp/workload-kubeconfig -v10 --dry-run
clusterctl -n rizomo-prod move --to-kubeconfig /tmp/workload-kubeconfig -v10
export KUBECONFIG=$WORKLOAD_CLUSTER_KUBECONFIG
clusterctl -n rizomo-prod describe cluster rizomo-prod
```

## Backup the cluster

The idea is to store all the state in buckets.
For databases, dumps, wal and/or physical snapshots are sent to buckets, we call this the first line.

We assume that at this point, you have a [rw oos user](https://gitlab.mim-libre.fr/rizomo/cluster-prod/-/tree/main/rizomo/preprod#create-aksk-for-oos), and another oos user that we'll call ro, and we'll configure everything in this section.

The tools (aws cli, and mc cli mainly) are configured with the read/write profile (rw) and read only (ro) profile.

First, you need to figure out the USER ID of both.
Create a random test bucket for that:
```
mc mb rw/rwrandombucket
mc mb ro/rorandombucket
```

And then save the USER ID in a variable:
```
rw_id=`aws s3api get-bucket-acl --profile rw --bucket rwrandombucket --endpoint https://oos.cloudgouv-eu-west-1.outscale.com | jq -r '.Grants[0].Grantee.ID'`
ro_id=`aws s3api get-bucket-acl --profile ro --bucket rorandombucket --endpoint https://oos.cloudgouv-eu-west-1.outscale.com | jq -r '.Grants[0].Grantee.ID'`
```

Then, for each prod bucket, you have to grant ro to ro user:
```
mc ls rw | cut -d" " -f 9 | cut -d'/' -f1 | xargs -L1 -I% aws s3api put-bucket-acl --profile rw --bucket % --endpoint https://oos.cloudgouv-eu-west-1.outscale.com --grant-full-control "id=${rw_id}" --grant-read "id=${ro_id}"
```

You can verify that this works with this command:
```
mc ls rw | cut -d" " -f 9 | cut -d'/' -f1 | xargs -L1 -I% aws s3api get-bucket-acl --profile rw --bucket % --endpoint https://oos.cloudgouv-eu-west-1.outscale.com 
```

## Upgrades

### cluster API

Upstream cluster-api documentation is here: https://cluster-api.sigs.k8s.io/clusterctl/commands/upgrade
Outscale cluster-api documentation is here: https://cluster-api-outscale.oos-website.eu-west-2.outscale.com/topics/upgrade-cluster.html

There are 4 components:

- upstream cluster api:
  - capi-kubeadm-bootstrap-controller-manager
  - capi-kubeadm-control-plane-controller-manager
  - capi-controller-manager
- outscale:
  - cluster-api-provider-outscale-controller-manager

Before the upgrade, check:
- matrix compatibility
- logs of all components and search for errors
After the upgrade, check:
- logs of all components and search for errors

It is possible to rollback.

You can also use this command to upgrade individually to a specific version:

```
clusterctl upgrade apply --core capi-system/cluster-api:v1.6.2
clusterctl upgrade apply --bootstrap capi-kubeadm-bootstrap-system/kubeadm:v1.6.2
clusterctl upgrade apply --control-plane capi-kubeadm-control-plane-system/kubeadm:v1.6.2
clusterctl upgrade apply --infrastructure cluster-api-provider-outscale-system/outscale:v0.3.1
```

### OS and kubernetes components

#### Control plane

Documentation is here:
https://cluster-api-outscale.oos-website.eu-west-2.outscale.com/topics/upgrade-cluster.html#upgrade-control-plane

#### Workers

Documentation is here:
https://cluster-api-outscale.oos-website.eu-west-2.outscale.com/topics/upgrade-cluster.html#upgrade-worker

### Cluster components

Check above to see how it was installed, and to upgrade: 

#### CNI

```
helm upgrade --install cilium cilium/cilium --version 1.15.1 --namespace kube-system -f cilium.yaml 
```

#### CSI

```
helm upgrade --install osc-bsu-csi-driver ./osc-bsu-csi-driver     --namespace kube-system     --set enableVolumeScheduling=true     --set enableVolumeResizing=true     --set enableVolumeSnapshot=true     --set region=cloudgouv-eu-west-1
```

#### CCM

```
helm upgrade --install -n kube-system --wait --wait-for-jobs k8s-osc-ccm deploy/k8s-osc-ccm --set oscSecretName=osc-secret
```

### libre.sh

Refer to upstream documentation:
https://forge.liiib.re/indiehost/libre.sh/libre.sh/-/tree/develop?ref_type=heads#upgrade
