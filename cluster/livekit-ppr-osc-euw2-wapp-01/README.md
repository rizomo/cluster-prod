This cluster doesn't have a local minio cluster running.

But libre.sh operator is running, and the bucket object is present.

For observability purpose, we use buckets from the minio deployed in  lasuite-ppr-osc-euw2-wapp-01.

When you deploy thanos kustomize, it is not happy, as the bucket is not provisionned. The idea is to scale down the lsh operator, make the bucket object as ready:

```
k edit --subresource=status bucket thanos
# add this condition
  status:
    conditions:
    - lastTransitionTime: "2025-01-22T16:12:16Z"
      message: Up and running
      observedGeneration: 1
      reason: Succeeded
      status: "True"
      type: Ready
# and you can force reconcile the kustomize flux resource:
flux -n libresh-system suspend ks thanos && flux -n libresh-system resume ks thanos
```
