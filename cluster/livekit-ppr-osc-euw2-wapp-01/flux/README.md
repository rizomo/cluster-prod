# Install libre.sh

```
# Add libre.sh operator
# "cd" https://forge.liiib.re/indiehost/libre.sh/libre.sh/
nix-shell
make install
make deploy
```

Create bucket in lasuite-ppr-osc-euw2-wapp-01 cluster.
Copy secrets from lasuite-ppr-osc-euw2-wapp-01 cluster in this cluster.
```
kubectx lasuite-ppr-osc-euw2-wapp-01
k get secret loki-chunks.bucket.libre.sh -o json | jq 'del(.metadata.creationTimestamp, .metadata.managedFields, .metadata.namespace, .metadata.ownerReferences, .metadata.resourceVersion, .metadata.selfLink, .metadata.uid, .status)' | jq '.metadata.namespace = "libresh-system"' > loki-chunks.json
k get secret thanos.bucket.libre.sh -o json | jq 'del(.metadata.creationTimestamp, .metadata.managedFields, .metadata.namespace, .metadata.ownerReferences, .metadata.resourceVersion, .metadata.selfLink, .metadata.uid, .status)' | jq '.metadata.namespace = "libresh-system"' > thanos.json 
k get secret loki-ruler.bucket.libre.sh -o json | jq 'del(.metadata.creationTimestamp, .metadata.managedFields, .metadata.namespace, .metadata.ownerReferences, .metadata.resourceVersion, .metadata.selfLink, .metadata.uid, .status)' | jq '.metadata.namespace = "libresh-system"' > loki-ruler.json

kubectx livekit-ppr-osc-euw2-wapp-01-admin@livekit-ppr-osc-euw2-wapp-01
k create ns libresh-system
k apply -f loki-ruler.json
k apply -f loki-chunks.json
k apply -f thanos.json
```
