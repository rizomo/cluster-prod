# Setup

## Deploy default flux folder

```
cd default-flux
k diff -k ./
k apply -k ./
```

## Create ns for livekit-ppr-osc-euw2-wapp-01 buckets
```
k create ns livekit-ppr-osc-euw2-wapp-01
```

## Deploy all the resources for this cluster

Instructions to deploy well the minio is here:
https://forge.liiib.re/indiehost/libre.sh/libre.sh/#deploy-minio-tenant

```
k diff -k ./
k apply -k ./
```

Copy the minio secret in libresh-config cofing map, as instructed here:
https://forge.liiib.re/indiehost/libre.sh/libre.sh/#configure-libresh-system

And then, you can deploy the libresh operator:

```
# "cd" https://forge.liiib.re/indiehost/libre.sh/libre.sh/
nix-shell
make install
make deploy
```
