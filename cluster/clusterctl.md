# Install clusterctl

(These instructions are for reference, and not meant for automation, refer to original documentation for up to date commands)

## Install outscale cluster api

https://github.com/outscale-dev/cluster-api-provider-outscale/blob/main/docs/src/topics/get-started-with-clusterctl.md
```
curl -L https://github.com/kubernetes-sigs/cluster-api/releases/download/v1.8.3/clusterctl-linux-amd64 -o clusterctl
sudo install -o root -g root -m 0755 clusterctl /usr/local/bin/clusterctl
clusterctl version
```

Please create $HOME/.cluster-api/clusterctl.yaml:

```
providers:
- name: outscale
  type: InfrastructureProvider
  url: https://github.com/outscale/cluster-api-provider-outscale/releases/latest/infrastructure-components.yaml
```

## Configure outscale credentials

Choose your Region:
```
export OSC_REGION=cloudgouv-eu-west-1
export OSC_REGION=eu-west-2
```

And create credentials:
```
export OSC_ACCESS_KEY=<your-access-key>
export OSC_SECRET_KEY=<your-secret-access-key>
kubectl create namespace cluster-api-provider-outscale-system
kubectl create secret generic cluster-api-provider-outscale --from-literal=access_key=${OSC_ACCESS_KEY} --from-literal=secret_key=${OSC_SECRET_KEY} --from-literal=region=${OSC_REGION}  -n cluster-api-provider-outscale-system
```

### In cloudgouv:

Configure [osc-cli](https://docs.outscale.com/en/userguide/Installing-and-Configuring-OSC-CLI.html) and make sure your bastion host can discuss with outscale API:

```
osc-cli api CreateApiAccessRule --IpRanges '["KindBastionIP/32"]'  --Description "Access from bastion-rizomo-prod"
```

(Full doc for [CreateApiAccessRule](https://docs.outscale.com/api?console#createapiaccessrule))
