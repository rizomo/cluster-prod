# Install libre.sh

```
# Create libresh-system
kubectl create ns libresh-system
# Install flux
kubectl create -f https://github.com/fluxcd/flux2/releases/latest/download/install.yaml

# requirements for observability
kubectl create secret -n libresh-system generic loki.admin.creds --from-literal=username=admin --from-literal=password=$(cat /dev/random | tr -dc '[:alnum:]' | head -c 40)
kubectl create secret -n libresh-system generic grafana.admin.creds --from-literal=username=admin --from-literal=password=$(cat /dev/random | tr -dc '[:alnum:]' | head -c 40)

# deploy
kubectl apply -k ./
```
